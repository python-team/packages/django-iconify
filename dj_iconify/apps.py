from django.apps import AppConfig


class DjIconifyConfig(AppConfig):
    name = "dj_iconify"
    label = "iconify"
