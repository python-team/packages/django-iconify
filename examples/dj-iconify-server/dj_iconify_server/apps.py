from django.apps import AppConfig


class DjIconifyServerConfig(AppConfig):
    name = "dj_iconify_server"
